extends Area2D

var kitty: KinematicBody2D

func _ready():
	kitty = get_node("/root/mvp/kitty")

func _physics_process(_delta):
	if overlaps_body(kitty):
		get_parent().gameOver()
