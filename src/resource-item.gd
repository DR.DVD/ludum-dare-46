extends Area2D

func _ready():
	add_to_group("resources")
	self.connect("area_entered", self, "makeKittyClimb")
	self.connect("area_exited", self, "stopKittyClimb")

func makeKittyClimb(_area):
	var kitty = get_node("/root/mvp/kitty")
	kitty.climbing = true

func stopKittyClimb(_area):
	var kitty = get_node("/root/mvp/kitty")
	kitty.climbing = false
