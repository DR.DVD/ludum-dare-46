extends Node2D

var cell_size = Vector2(128, 128)
var isPlayerDead = false
var hasPlayerWon = false

var resources = []
var selectedResource

var map: TileMap

func _ready():
	var screen_size: Vector2 = OS.get_screen_size()
	var window_size: Vector2 = OS.get_window_size()
	OS.set_window_position(screen_size * 0.5 - window_size * 0.5)
	map = get_node("gridmap")

	resources = get_tree().get_nodes_in_group("resources")
	selectedResource = resources[0]

func _input(_event):
	if isPlayerDead == true && Input.is_action_just_pressed("ui_accept"):
		get_tree().reload_current_scene()
	elif Input.is_action_just_pressed("item_place"):
		var mouse_pos = get_global_mouse_position()
		var selectedResourcePos = Vector2()
		selectedResourcePos.x = floor(mouse_pos.x / cell_size.x) * cell_size.x
		selectedResourcePos.y = floor(mouse_pos.y / cell_size.y) * cell_size.y
		if selectedResource.name == "pillow":
			selectedResource.position = selectedResourcePos
		elif selectedResource.name == "ladder":
			var cell_pos = map.world_to_map(Vector2(selectedResourcePos.x, selectedResourcePos.y + cell_size.y * 2))
			var used_cells = map.get_used_cells()
			if used_cells.find(cell_pos) != -1:
				selectedResource.position = selectedResourcePos
#		for cell in get_used_cells():
#			var cell_pos = map_to_world(Vector2(cell.x, cell.y))
#			var cell_rectangle = Rect2(cell_pos, Vector2(cell_size.x, cell_size.y))
##			print("cellpos!", cell_pos.x, ", ", cell_pos.y)
#			if cell_rectangle.has_point(mouse_pos):
#				print("Success!")

func gameOver():
	if isPlayerDead == false:
		var uiDead = get_node("/root/mvp/ui/CenterContainer/message_dead")
		var kitty = get_node("kitty")
		uiDead.visible = true
		isPlayerDead = true
		kitty.die()

func win():
	if hasPlayerWon == false:
		var ui = get_node("/root/mvp/ui/ui-control")
		var uiWin = get_node("/root/mvp/ui/CenterContainer/message_win")
		var victorySound = preload("res://assets/music/victory_sound.wav")
		var music = get_tree().get_root().get_children()[0]
		music.stream = victorySound
		music.play()
		ui.visible = false
		uiWin.visible = true
		hasPlayerWon = true
