extends KinematicBody2D

var mvp: Node2D

export var speed = Vector2(300, 900)
export var gravity = 600
const acceleration = 600
const jumpGravity = -400

var velocity = Vector2.ZERO
var climbVelocity = Vector2(0, -200)
const floorNormal = Vector2(0, -1)

var dead = false
var climbing = false
var won = false

func _ready():
	mvp = get_parent()

func _physics_process(delta):
	if dead == false:
		if get_slide_count() > 0:
			for i in get_slide_count():
				var collision = get_slide_collision(i)
				if collision.collider.name == "enemy":
					mvp.gameOver()
				elif collision.collider.name == "winning":
					mvp.win()
		if climbing == false:
			if is_on_floor():
				velocity.y -= floor(gravity * delta)
				velocity.x += floor(acceleration * delta)
				if velocity.x > speed.x:
					velocity.x = speed.x
				if velocity.y < 0:
					velocity.y = 0
				if is_on_wall():
					jump()
			else:
				velocity.y += floor(gravity * delta)
				if velocity.y > speed.y:
						velocity.y = speed.y
		else:
			velocity = climbVelocity
		move_and_slide(velocity, floorNormal)

func jump():
	velocity.x = 100
	velocity.y = jumpGravity
	pass

func die():
	dead = true
	var tween = get_node("Tween")
	tween.interpolate_property(self, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), 0.5)
	tween.start()
	var player = AudioStreamPlayer.new()
	add_child(player)
	player.stream = load("res://assets/music/death_sound.wav")
	player.play()
